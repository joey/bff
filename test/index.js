import assert from "assert"
import index from "../somedomain/endpoint.mjs"
describe("Endpoints", ()=>{
    it("should say hello world", done=>{
        assert.ok(index.response === "Hello World!")
        done()
    })
})
