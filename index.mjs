import express from "express"
import endpoint from "./somedomain/endpoint.mjs"
const app = express()
import http from "http"
const api_url = `http://${process.env.API}`
let connections = []
let server = null
const request = url => {
    return new Promise((resolve, reject) => {
        http.get(url, res => {
            let error = null
            if (res.statusCode >= 299) error = new Error(`Request ${url} Failed with Status Code: ${res.statusCode}`);
            if (error) {
                res.resume()
                return reject(error)
            }
            res.setEncoding('utf8')
            let data = ""
            res.on("data", chunk => data += chunk)
            res.on("end", () => {
                try {
                    resolve(JSON.parse(data))
                } catch (e) {
                    reject(e);
                }
            })
        }).on("error", e => reject(e))
    })
}

const shutDown = () => {
    console.log("Received kill signal, shutting down gracefully")
    server.close(() => {
        console.log("Closed out remaining connections")
        process.exit(0)
    });

    setTimeout(() => {
        console.error("Could not close connections in time, forcefully shutting down")
        process.exit(1)
    }, 10000);
    connections.forEach(con => con.end());
    connections.forEach(con => con.destroy())
    process.exit(0)
}

app.on("connection", connection => {
    connections.push(connection);
    connection.on("close", () => connections = connections.filter(curr => curr !== connection));
})
app.get(["/healthz", "/bff/healthz"], async (req, res) => {
    res.json({health: "good"})
})
app.get(["/", "/bff"], async (req, res) => {
    let obj = null
    try{
        obj = await request(`${api_url}/backend`)
    } catch(e){
        console.log("Request failed", e)
        obj = {error: `${e.name}: ${e.message}`}
    }
    res.json(obj)
})
const PORT = process.env.NOMAD_PORT_http || process.env.PORT
server = app.listen(PORT, () => console.log(`bff http://localhost:${PORT}`))
process.on("SIGTERM", shutDown)
process.on("SIGINT", shutDown)
