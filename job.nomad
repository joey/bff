#A unique name four our service
job "product_listing" {
  #The datacenter in which the job's tasks will be running.
  datacenters = ["dc1"]
  #Series of task that should be colocated in the same nomad client.
  group "mac-minis" {
    #The number of task groups that should be running under this group.
    count = 1
    restart {
      attempts = 10
      interval = "5m"
      delay = "25s"
      mode = "delay"
    }
  #A task represents an individual unit of work. This task uses a docker container.
  task "bff" {
    #The driver that should be use the nomad client to execute the task.
    driver = "raw_exec"
    #Configuration specific to the task.
    config {
      command = "npm"
      args = ["start"]
    }
    artifact {
      source  = "/Users/jguerra/code/bff/bff.zip"
      destination = "/Users/jguerra/code/bff/build"
    }
  #The service section indicates to nomad how to register the task with consul for discovery and monitoring.
  service {
    name = "bff"
    #These tags are published into consul and announces the path the upstream service accept.
    #Fabio monitors the tags, and if urlprefix is detected,
    #then it will include a route for the service.
    tags = ["urlprefix-/bff", "bff"]
    #Specifies the port to advertise the service. The port could be a value or a port label.
    port = "http"
    #Health check four our service. By default fabio only watches services which have a passing health check.
    check {
      type = "http"
      path = "/healthz"
      interval = "10s"
      timeout = "2s"
     }
  }
  env {
    API = "fabio:9999/backend"
  }
  #Requirements for the task including memory, network, CPU, and more.
  resources {
     cpu = 500 # 500 Mhz
     memory = 64 # 64MB
     network {
       mbits = 10
       port "http" {}
     }
    }
   }
  }
}